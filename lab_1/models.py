from django.db import models


# TODO Create Friend model that contains name, npm, and DOB (date of birth) here


class Friend(models.Model):
    name = models.CharField(max_length=30, null=True, blank=True)
    npm = models.CharField(max_length = 10, null=True, blank=True)
    birth_date = models.DateField(null=True, blank=True)
