from django.db import models

# Create your models here.

class Note(models.Model):
    Titles = models.CharField(max_length=50)
    To = models.CharField(max_length=30)
    From = models.CharField(max_length=30)
    Message = models.CharField(max_length=1000)
