from django.urls import path, re_path, include
from .views import index, add_note, note_list

urlpatterns = [
    path('', index, name='index'),
    path('add', add_note, name='Add'),
    path('card', note_list, name='NoteList')
]

