from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect

# Create your views here.
def index(request):
    note = Note.objects.all()  # TODO Implement this
    response = {'note': note}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context ={}
  
    # create object of form
    form = NoteForm(request.POST or None, request.FILES or None)
      
    # check if form data is valid
    if form.is_valid():
        # save the form data to model
        form.save()
        return HttpResponseRedirect('/lab-4')
  
    context['form']= form
    return render(request, "lab4_form.html", context)

def note_list(request):
    note = Note.objects.all()  # TODO Implement this
    response = {'note': note}
    return render(request, 'lab4_note_list.html', response)