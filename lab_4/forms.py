from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['To', 'From', 'Titles', 'Message']
    error_messages = {
        "To" : { "required" : "Lengkapi Input", },
        "From" : { "required" : "Lengkapi Input", },
        "Titles" : { "required" : "Lengkapi Input", },
        "Message" : { "required" : "Lengkapi Input", },
    }
    input_attrs = {
        "To" :{   "type" : "text",
                    "placeholder" : "Enter name",
                },
        "From" :{    "type" : "text",
                    "placeholder" : "Enter name",
                },
        "Titles" :{  "type" : "text",
                        "placeholder" : "Enter title",
                },
        "Message" :{  "type" : "text",
                        "placeholder" : "Enter message",
                },
    }