import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('Stotes'),
            backgroundColor: Colors.lime[700],
            actions: <Widget>[
              FlatButton(
                textColor: Colors.white,
                onPressed: () {},
                child: Text("Home"),
                shape:
                    CircleBorder(side: BorderSide(color: Colors.transparent)),
              ),
              FlatButton(
                textColor: Colors.white,
                onPressed: () {},
                child: Text("About Us"),
                shape:
                    CircleBorder(side: BorderSide(color: Colors.transparent)),
              ),
              FlatButton(
                textColor: Colors.white,
                onPressed: () {},
                child: Text("Login"),
                shape:
                    CircleBorder(side: BorderSide(color: Colors.transparent)),
              ),
            ],
          ),
          body: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Image(
                    image: AssetImage('assets/task-view.png'),
                    width: 250,
                    height: 250),
                Text(
                  'Working from home has never been this easy.'
                  '\n'
                  '\nFeel the benefits and be more productive with us.',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ])),
    ));
