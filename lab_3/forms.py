from django import forms
from lab_1.models import Friend

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'npm', 'birth_date']
    error_messages = {
        "name" : { "required" : "Lengkapi Input", },
        "npm" : { "required" : "Lengkapi Input", },
        "birth_date" : {
        "required" : "Lengkapi Input",
        "invalid" : "Format Tanggal Anda Salah"},
    }
    input_attrs = {
        "name" :{   "type" : "text",
                    "placeholder" : "Your Name",
                },
        "npm" :{    "type" : "number",
                    "placeholder" : 0000000000,
                },
        "birth_date" :{  "type" : "date",
                        "placeholder" : "YYYY-MM-DD",
                },
    }
    
